package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll(final String userId);

    List<Project> findAll(final String userId, final Comparator<Project> comparator);

    Project findById(final String userId, final String id);

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final int index);

    void add(final String userId, final Project project);

    void remove(final String userId, final Project project);

    Project removeById(final String userId, final String id);

    Project removeByName(final String userId, final String name);

    Project removeByIndex(final String userId, final int index);

    void clear(final String userId);

    int getSize(final String userId);
}
