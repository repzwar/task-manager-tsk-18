package ru.pisarev.tm.api.service;

import ru.pisarev.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll(final String userId);

    List<Project> findAll(final String userId, final Comparator<Project> comparator);

    Project findById(final String userId, final String id);

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final Integer index);

    Project add(final String userId, final Project project);

    void remove(final String userId, final Project project);

    Project removeById(final String userId, final String id);

    Project removeByName(final String userId, final String name);

    Project removeByIndex(final String userId, final Integer index);

    void clear(final String userId);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

    Project startById(final String userId, final String id);

    Project startByIndex(final String userId, final Integer index);

    Project startByName(final String userId, final String name);

    Project finishById(final String userId, final String id);

    Project finishByIndex(final String userId, final Integer index);

    Project finishByName(final String userId, final String name);

}
