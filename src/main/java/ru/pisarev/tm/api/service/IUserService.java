package ru.pisarev.tm.api.service;

import ru.pisarev.tm.model.User;

import java.util.List;

public interface IUserService {
    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User add(String login, String password);

    User add(String login, String password, String email);

    User setPassword(String id, String password);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );
}
