package ru.pisarev.tm.service;

import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.service.ITaskService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final String userId) {
        return taskRepository.findAll(userId);
    }

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(userId, comparator);
    }

    @Override
    public Task findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > taskRepository.getSize(userId)) throw new IndexIncorrectException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public Task findByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task add(final String userId, final Task task) {
        if (task == null) return null;
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public Task removeById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(userId, id);
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @Override
    public void clear(final String userId) {
        taskRepository.clear(userId);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(userId, id);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(userId, name);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(userId, id);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(userId, name);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

}
