package ru.pisarev.tm.service;

import ru.pisarev.tm.api.service.IUserService;
import ru.pisarev.tm.exception.empty.EmptyLoginException;
import ru.pisarev.tm.exception.empty.EmptyPasswordException;
import ru.pisarev.tm.exception.system.AccessDeniedException;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.HashUtil;

public class AuthService implements ru.pisarev.tm.api.service.IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.add(login, password, email);
    }
}
