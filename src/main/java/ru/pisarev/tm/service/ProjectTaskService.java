package ru.pisarev.tm.service;

import ru.pisarev.tm.api.repository.IProjectRepository;
import ru.pisarev.tm.api.repository.ITaskRepository;
import ru.pisarev.tm.api.service.IProjectTaskService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;


    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskById(final String userId, final String taskId, final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.bindTaskToProjectById(userId, taskId, projectId);
    }

    @Override
    public Task unbindTaskById(final String userId, final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    public Project removeProjectById(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        String projectId = projectRepository.findByIndex(userId, index).getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @Override
    public Project removeProjectByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        String projectId = projectRepository.findByName(userId, name).getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

}