package ru.pisarev.tm.command.auth;

import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.util.TerminalUtil;

public class LoginCommand extends AuthAbstractCommand {

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logging in to application.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
