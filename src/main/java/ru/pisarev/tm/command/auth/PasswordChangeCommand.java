package ru.pisarev.tm.command.auth;

import ru.pisarev.tm.command.AuthAbstractCommand;
import ru.pisarev.tm.model.User;
import ru.pisarev.tm.util.TerminalUtil;

public class PasswordChangeCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "password-change";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter new password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(user.getId(), password);
    }
}
