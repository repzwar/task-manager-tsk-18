package ru.pisarev.tm.command.project;

import ru.pisarev.tm.command.ProjectAbstractCommand;
import ru.pisarev.tm.model.User;

public class ProjectClearCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        serviceLocator.getProjectService().clear(user.getId());
    }
}
