package ru.pisarev.tm.command.system;

import ru.pisarev.tm.command.AbstractCommand;

public class AboutDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("Aleksey Pisarev");
        System.out.println("pisarevaleks@mail.ru");
    }
}
