package ru.pisarev.tm.exception.empty;

import ru.pisarev.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty");
    }

}
