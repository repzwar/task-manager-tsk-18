package ru.pisarev.tm.exception.empty;

import ru.pisarev.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error. Password is empty");
    }

}
